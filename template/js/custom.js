// JavaScript Document

'use strict';
var noconf=jQuery.noConflict();


noconf(document).ready(function(e) {
  noconf( "#result" ).click(function() {
     noconf( "#result" ).hide();
});

  
  
  noconf('.onscroll-animate').each(function(index, element) {
    noconf(this).one('inview', function (event, visible) {
      var el = noconf(this);
      if(el.hasClass('graph'))
        var anim = "graph-anim";
      else
        var anim = (el.data("animation") !== undefined) ? el.data("animation") : "fadeIn";
      var delay = (el.data("delay") !== undefined ) ? el.data("delay") : 200;
        
      setTimeout(function() {
        el.addClass(anim);
        if(el.hasClass('graph'))
          el.children('.graph-value').countTo();
      }, delay);
    });
  });
  
  noconf(document).on( 'scroll', function(){
    checkHeaderScroll();
  });
  
  function checkHeaderScroll() {
    var offset = noconf('#head').height() - 80;
    noconf('#header-static').removeClass('after-scroll');
    if(noconf(window).scrollTop() > offset) {
      noconf('#header-static').addClass('after-scroll');
    }
  }
  
});


var $ = jQuery.noConflict();
$(document).ready(function(){

     // Устанавливаем обработчик потери фокуса для всех полей ввода текста
     $('input#user_login, input#email, input#user_name, input#password, #message').blur( function(){

        // Для удобства записываем обращения к атрибуту и значению каждого поля в переменные
         var id = $(this).attr('id');
         var val = $(this).val();

       // После того, как поле потеряло фокус, перебираем значения id, совпадающие с id данного поля
       switch(id)
       {
             // Проверка поля "Имя"
             case 'user_login':
                var rv_name = /^[a-zA-Z0-9-.]+$/; // используем регулярное выражение

                // Eсли длина имени больше 2 символов, оно не пустое и удовлетворяет рег. выражению,
                // то добавляем этому полю класс .not_error,
                // и ниже в контейнер для ошибок выводим слово " Принято", т.е. валидация для этого поля пройдена успешно

                if(val != '' && rv_name.test(val))
                { 
                  $('.form .error-text .error_login').html('');
                  $('.form .error-text .error_login').fadeOut(300);
            }
                else
                {
                  $('.form .error-text .error_login').fadeIn(300);
                   $('.form .error-text .error_login').html('Допустимый ввод в поле "Логин" английские буквы и цифры');
                }
            break;

            case 'user_name':

                if(val != '')
                { 
                  $('.form .error-text .error_user_name').html('');
                  $('.form .error-text .error_user_name').fadeOut(300);
            }
                else
                {
                  $('.form .error-text .error_user_name').fadeIn(300);
                   $('.form .error-text .error_user_name').html('Пожалуйста введите Ваше имя');
                }
            break;

            case 'password':

                if(val != '')
                { 
                  $('.form .error-text .error_password').html('');
                  $('.form .error-text .error_password').fadeOut(300);
            }
                else
                {
                  $('.form .error-text .error_password').fadeIn(300);
                   $('.form .error-text .error_password').html('Пожалуйста введите Ваш пароль');
                }
            break;

           // Проверка email
           case 'email':
               var rv_email = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
               if(val != '' && rv_email.test(val))
               {
                $('.form .error-text .error_email').html('');
                $('.form .error-text .error_email').fadeOut(300);
               }
               else
               {
                $('.form .error-text .error_email').fadeIn(300);
                  $('.form .error-text .error_email').html('Поле "Email" обязательно для заполнения, поле должно содержать правильный email-адрес<br>');
               }
           break;

           case 'message':
                if(val != '')
                { 
                  $('.form .error-text .error_message').html('');
                  $('.form .error-text .error_message').fadeOut(300);
            }
                else
                {
                  $('.form .error-text .error_message').fadeIn(300);
                   $('.form .error-text .error_message').html('Поле "Message" обязательно для заполнения');
                }
           break;

       } 

     });



  $('.faq_block li').on('click', function() {
    if ($(this).hasClass('active')) {
      return false;
    } else {
      $('.faq_block li').removeClass('active');
      $(this).addClass('active');
      $('.faq_block li p').slideUp(600);
      $('p',this).slideDown(600);
    }
  });


    $('.form-reg').submit(function() {
        if ($('.error-text .error_user_name').text() == '' && $('.error-text .error_login').text() == '' && $('.error-text .error_email').text() == '' && $('.error-text .error_password').text() == '' && $('#user_name').val() != '' && $('#user_login').val() != '' && $('#email').val() != '' && $('#password').val() != '') {
          console.log('222');
          var user_name = $('#user_name').val(),
                  user_login = $('#user_login').val(),
                  email = $('#email').val(),
                  password = $('#password').val(),
                  partner = $('#partner').val();
            $.post('add_new_order.php',{user_name:user_name,user_login:user_login,email:email,password:password, partner:partner},'json');
        } else {
          if ($('#user_name').val() == '') {
            $('.form .error-text .error_user_name').fadeIn(300);
            $('.form .error-text .error_user_name').html('Пожалуйста введите Ваше имя');
          }
          if ($('#user_login').val() == '') {
            $('.form .error-text .error_login').fadeIn(300);
            $('.form .error-text .error_login').html('Допустимый ввод в поле "Логин" английские буквы и цифры');
          }
          if ($('#email').val() == '') {
            $('.form .error-text .error_email').fadeIn(300);
            $('.form .error-text .error_email').html('Поле "Email" обязательно для заполнения, поле должно содержать правильный email-адрес<br>');
          }
          if ($('#password').val() == '') {
            $('.form .error-text .error_password').fadeIn(300);
            $('.form .error-text .error_password').html('Пожалуйста введите Ваш пароль');
          }
          return false;

        }
      });

      $('.form-auth').submit(function() {
        
          if ($('#user_login').val() == '') {
            $('.form .error-text .error_login').fadeIn(300);
            $('.form .error-text .error_login').html('Допустимый ввод в поле "Логин" английские буквы и цифры');
          }
          if ($('#password').val() == '') {
            $('.form .error-text .error_password').fadeIn(300);
            $('.form .error-text .error_password').html('Пожалуйста введите Ваш пароль');
          }
          if ($('#password').val() == '' || $('#user_login').val() == '') {
            return false;
          }

      });

      $('.form_forg ').submit(function() {
        
          if ($('#user_login').val() == '') {
            $('.form .error-text .error_login').fadeIn(300);
            $('.form .error-text .error_login').html('Допустимый ввод в поле "Логин" английские буквы и цифры');
          }
          if ($('#email').val() == '') {
            $('.form .error-text .error_email').fadeIn(300);
            $('.form .error-text .error_email').html('Поле "Email" обязательно для заполнения, поле должно содержать правильный email-адрес<br>');
          }
          if ($('#email').val() == '' || $('#user_login').val() == '') {
            return false;
          }

      });

      $('.form_supp ').submit(function() {
        
          if ($('#user_login').val() == '') {
            $('.form .error-text .error_login').fadeIn(300);
            $('.form .error-text .error_login').html('Допустимый ввод в поле "Логин" английские буквы и цифры');
          }
          if ($('#email').val() == '') {
            $('.form .error-text .error_email').fadeIn(300);
            $('.form .error-text .error_email').html('Поле "Email" обязательно для заполнения, поле должно содержать правильный email-адрес<br>');
          }
          if ($('#message').val() == '') {
            $('.form .error-text .error_message').fadeIn(300);
            $('.form .error-text .error_message').html('Поле "Message" обязательно для заполнения');
          }

          if ($('#email').val() == '' || $('#user_login').val() == '' || $('#message').val() == '') {
            return false;
          }

      });

  }); 

$(function(){

  $('body.chrome select, body.safari select').each(function(){
    var tag = st(this).parent().prop('tagName');
      if ( tag !== 'LABEL' ) {
        st(this).wrap('<label class="st-select-label"></label>');
      };
  })
  

  $('ul.tabs li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('selected');
    $('.inner-content').removeClass('selected');

    $(this).addClass('selected');
    $('#'+tab_id).addClass('selected');

  });  

  $(".filter-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});
 
