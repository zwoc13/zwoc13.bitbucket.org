/* flip plugin */

!function(a){var b=function(a){a.data("fliped",!0);var b="rotate"+a.data("axis");a.find(".front").css({transform:b+(a.data("reverse")?"(-180deg)":"(180deg)")}),a.find(".back").css({transform:b+"(0deg)"})},c=function(a){a.data("fliped",!1);var b="rotate"+a.data("axis");a.find(".front").css({transform:b+"(0deg)"}),a.find(".back").css({transform:b+(a.data("reverse")?"(180deg)":"(-180deg)")})};a.fn.flip=function(d){return this.each(function(){var e=a(this);if(void 0!==d&&"boolean"==typeof d)d?b(e):c(e);else{var f=a.extend({axis:"y",reverse:!1,trigger:"click",speed:500},d);if(e.data("reverse",f.reverse),e.data("axis",f.axis),"x"==f.axis.toLowerCase())var g=2*e.outerHeight(),h="rotatex";else var g=2*e.outerWidth(),h="rotatey";e.find(".back").css({transform:h+"("+(f.reverse?"180deg":"-180deg")+")"}),e.css({perspective:g,position:"relative"});var i=f.speed/1e3||.5;if(e.find(".front, .back").outerHeight(e.height()).outerWidth(e.width()).css({"transform-style":"preserve-3d",position:"absolute",transition:"all "+i+"s ease-out","backface-visibility":"hidden"}),"click"==f.trigger.toLowerCase())e.find('button, a, input[type="submit"]').click(function(a){a.stopPropagation()}),e.click(function(){e.data("fliped")?c(e):b(e)});else if("hover"==f.trigger.toLowerCase()){var j=function(){e.unbind("mouseleave",k),b(e),setTimeout(function(){e.bind("mouseleave",k),e.is(":hover")||c(e)},f.speed+150)},k=function(){c(e)};e.mouseenter(j),e.mouseleave(k)}}}),this}}(jQuery);

/* Custom code */

$(function(){
	function heightDetect() {
		$('#hero').css('height', $(window).height());
		$('#hero').css('width', $(window).width());
	}
	heightDetect();
	$(window).resize(function(){
		heightDetect();
	});

	$(window).load(function(){
		$('#item5').height($('#item4').height());

		$('#item-n1').flip({
		trigger: 'hover',
		axis: 'y',
		speed: 800
	});

	$('#item-n2').flip({
		trigger: 'hover',
		axis: 'x'
	});

	$('#item-n3').flip({
		trigger: 'hover',
		axis: 'y',
		reverse: 'true'
	});

	$('#item-n4').flip({
		trigger: 'hover',
		axis: 'x'
	});

	$('#item-n5').flip({
		trigger: 'hover',
		axis: 'y'
	});

	ratio1 = 1.224;
	ratio2 = 1.9519;	
	ratio3 = 1.39;
	ratio4 = 2.005;
	ratio5 = 0.9252;

	hw1 = $('#item1').width*ratio1;
	hw2 = $('#item2').width*ratio2;
	hw3 = $('#item3').width*ratio3;
	hw4 = this.width*ratio4;
	hw5 = this.width*ratio5;

	$('#item1').css('height', hw1); 
	$('.item-n1, .item-n1 .front-side, .item-n1 .back-side').height($('#item1').height());

	$('#item2').css('height', hw2);
	$('.item-n2, .item-n2 .front-side, .item-n2 .back-side').height($('#item2').height());

	$('#item3').css('height', hw2);
	$('.item-n3, .item-n3 .front-side, .item-n3 .back-side').height($('#item3').height());

	$('#item4').css('height', hw2);
	$('.item-n4, .item-n4 .front-side, .item-n4 .back-side').height($('#item4').height());

	$('#item5').css('height', hw2);
	$('.item-n5, .item-n5 .front-side, .item-n5 .back-side').height($('#item5').height());

	})

	if (screen.width <= 1920 && screen.width >= 1360) {
		$('footer').width($('.hero').width());
	}

})