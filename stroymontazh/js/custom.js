$(function() {
	var phone = $('input[name=phone]');
	var overlay = $('#overlay');
	var modal = $('#modal');

	phone.mask('+7 (000) 000-00-00');

	$('#overlay').click(function() {
		hideModal();
	});

	function showModal() {
		overlay.show();
		modal.show();
	};

	function hideModal() {
		modal.hide();
		overlay.hide();
	};

	$('.showmodal').click(function(){
		showModal();
	});
});

