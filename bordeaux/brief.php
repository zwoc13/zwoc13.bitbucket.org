<!DOCTYPE html>
<html>
<head>
	<title>The brief - Bordeaux Internation</title>
	<meta name="description" content="Mixed One Page Style 1">
    <meta name="author" content="pixel-industry">
	<meta name="keywords" content="CSS, HTML5, clean, corporate, jQuery, retina, bootstrap, business">
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="./img/bordeaux-icon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Stylesheets -->
        <link rel="stylesheet" href="css/bootstrap.css"/><!-- bootstrap grid -->
        <link rel="stylesheet" href="css/style.css"/><!-- template styles -->
        <link rel="stylesheet" href="css/color-default.css"/><!-- default template color styles -->
        <link rel="stylesheet" href="css/retina.css"/><!-- retina ready styles -->
        <link rel="stylesheet" href="css/animate.css"/><!-- animation for content -->
        <link rel="stylesheet" href="css/responsive.css"/><!-- responsive styles --> 
        <link rel="stylesheet" href="linecons/linecons.css"/><!-- line icons css -->
        <link rel="stylesheet" href="css/nivo-slider.css"/><!-- Nivo Slider styles -->
          <link rel='stylesheet' href='owl-carousel/owl.carousel.css'/><!-- .carousels -->

        <!-- Revolution slider -->
        <link rel="stylesheet" href="rs-plugin/css/settings.css"/><!-- .rs-settings end -->
        <link rel="stylesheet" href="rs-plugin/css/mixed.css"/><!-- settings for rev slider -->

        <!-- Maginic Popup - image lightbox -->
        <link rel="stylesheet" href="css/magnific-popup.css" />

        <!-- Google Web fonts -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,100,200,500,600,800,700,900' rel='stylesheet' type='text/css'><!-- Raleway font -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;.subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic' rel='stylesheet' type='text/css'><!-- Open Sans -->

        <!-- Font icons -->
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/><!-- Font awesome icons -->
        <link rel="stylesheet" href="pixons/style.css" /><!-- Social icons font - Pixons -->
</head>
<body style="background: #dedede;">
	<div id="header-wrapper" class="header-simple">
            <!-- #header.header-type-1 start -->
            <header id="header" class="header-type-1 dark">

                <!-- Main navigation and logo container -->
                <div class="header-inner">
                    <!-- .container start -->
                    <div class="container">
                        <!-- .main-nav start -->
                        <div class="main-nav">
                            <!-- .row start -->
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- .navbar.pi-mega start -->
                                    <nav class="navbar navbar-default nav-left pi-mega" role="navigation">									

                                        <!-- .navbar-header start -->
                                        <div class="navbar-header">
                                            <!-- .logo start -->
                                            <div class="logo">
                                                <a href="index.html"><img src="img/bordeaux.png"></a>
                                            </div><!-- logo end -->
                                        </div><!-- .navbar-header end -->

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <div class="collapse navbar-collapse"> <!--one-page-nav is working right now. Neede to add external classes both for <li> and <a>-->
                                            <ul class="nav navbar-nav pi-nav one-page-nav">
                                                <li class="current-menu-item"><a href='#section-home'>Home</a></li>                                                
                                                <li><a href='./index.html#section-about'>Portfolio</a></li>
                                                <li><a href='./index.html#section-team'>Team</a></li>
                                                <!--<li><a href='#section-portfolio'>Work</a></li>-->
                                                <li><a href='./index.html#section-services'>Services</a></li>
                                            <!--    <li><a href="#section-blog">Blog</a></li>-->
                                                <li class="external"><a class="external" href="./contact.html">Contact us</a></li>
                                            </ul><!-- .nav.navbar-nav.pi-nav end -->

                                         
                                            <div id="dl-menu" class="dl-menuwrapper one-page-nav">
                                                <button class="dl-trigger">Menu</button>

                                                <ul class="dl-menu">
                                                    <li><a href='./index.html#section-home'>Home</a></li>
                                                    <li><a href='./index.html#section-about'>Portfolio</a></li>
                                                    <li><a href='./index.html#section-team'>Team</a></li>
                                                    <!--<li><a href='#section-portfolio'>Work</a></li>-->
                                                    <li><a href='./index.html#section-services'>Services</a></li>
                                                   <!-- <li><a href="#section-blog">Blog</a></li>-->
                                                    <li class="external"><a class="external" href="./contact.html" >Contact us</a></li>
                                                </ul> 
                                            </div> 

                                            <!-- #search-box start -->

                                        </div><!-- .navbar.navbar-collapse end --> 
                                    </nav><!-- .navbar.pi-mega end -->
                                </div><!-- .col-md-12 end -->
                            </div><!-- .row end -->            
                        </div><!-- .main-nav end -->
                    </div><!-- .container end -->
                </div><!-- .header-inner end -->
            </header><!-- #header.header-type-1.dark end -->
        </div><!-- #header-wrapper end -->
		<section id="brief" class="container">
				<div class="col-sm-12">
					<h1>Web Development Brief</h1>
					<p>The main target of this documents – is to get all the requirement for the webpage. Empty fields have to be fully completed, so the designers and programmers could understand everything. If you have nothing to add to the field – leave it empty.</p>
					<div class="line"></div>
				</div>
				<form action="form.php" class="form-content">
				
					<h3>Tell us about yourself:</h3>
					<div class="form-group">
						<label class="col-sm-4" for="username">Full name:</label>
						<input type="text" class="col-sm-8">
					</div>
					<div class="form-group">
						<label class="col-sm-4" for="position">Position:</label>
						<input type="text" class="col-sm-8">
					</div>
					<div class="form-group">
						<label class="col-sm-4" for="cellphone">Phone number:</label>
						<input type="text" name="cellphone" class="col-sm-8">
					</div>
					<div class="form-group">
						<label for="skype" class="col-sm-4">Skype ID:</label>
						<input type="text" name="skype" class="col-sm-8">
					</div>
					<div class="line"></div>
					<h3>About company:</h3>
					<div class="form-group">
						<label class="col-sm-4" for="cellphone">Full company's name:</label>
						<input type="text" name="cellphone" class="col-sm-8">
					</div>
				<input type="submit" value="Send the Brief">
				</form>
		</section>

		 <div id="footer-wrapper">
            <!-- #footer start -->
            <footer id="footer">
                <!-- .container start -->
                <div class="container">
                    <!-- .row start -->
                    <div class="row">
                        <!-- .footer-widget-container start -->
                        <ul class="col-md-3 footer-widget-container">
                            <!-- .widget.widget_text -->
                            <li class="widget widget_newsletterwidget">
                                <div class="title">
                                    <h3>About Company</h3>
                                </div>

                                <p>
                                    We are ServiceNow specialists, developers and UI/Web designers
                                    focused on software development projects.
                                </p>

                                <br />

                               
                            </li><!-- .widget.widget_newsletterwidget end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->

                        <!-- .footer-widget-container start -->
                        <ul class="col-md-3 footer-widget-container">
                            <!-- .widget_pages start -->
                            <li class="widget widget_pages">
                                <div class="title">
                                    <h3>company information</h3>
                                </div>

                                <ul>
                                    <li><a href="about.html">About us</a></li>
                                    <li class="external"><a href="./partners.html">Partners</a></li>

                                    <li><a href="./contact.html">Contact</a></li>
                                </ul>
                            </li><!-- .widget.widget_pages end -->                            
                        </ul><!-- .footer-widget-container end -->
<ul class="col-md-3 footer-widget-container">
    <!-- #tweetscroll-wrapper start -->
    <li class="widget" id="tweetscroll-wrapper">
        <div class="title">
            <h3>latest tweets</h3>
        </div>
        <div class="tweets-list-container tweets-list-container-single-profile twitter-logo"></div>
    </li><!-- .widget#tweetscroll-wrapper end -->
</ul><!-- .col-md-3.footer-widget-container end -->


                        <ul class="col-md-3 footer-widget-container">
                            <!-- .widget.contact-info start -->
                            <li class="widget contact-info">
                                <div class="title">
                                    <h3>get in touch</h3>
                                </div>

                                <ul class="contact-info-list">
                                    <li>
                                        <i class="fa fa-home"></i>
                                        <strong>Address: </strong>Sunrise Street, 243 Avenue, Manhattan, NV City, NY
                                    </li>

                                    <li>
                                        <i class="fa fa-phone"></i>
                                        <strong>Phone: </strong>+00 4859 7512 1476
                                    </li>

                                    <li>
                                        <i class="fa fa-envelope-o"></i>
                                        <strong>Email: </strong><a href="mailto:info@business.com">info@business.com</a>
                                    </li>
                                </ul>
                            </li><!-- .widget.contact-info end -->
                        </ul><!-- .footer-widget-container end -->
                    </div><!-- .row end -->
                </div><!-- .container end -->
            </footer><!-- #footer end -->

            <a href="#" class="scroll-up">Scroll</a>
        </div><!-- #footer-wrapper end -->
</body>
</html>